/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholders Individual Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_Capture_Stakeholders_Individual_Actions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    
    public FR7_Capture_Stakeholders_Individual_Actions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());  
    }

    public TestResult executeTest()
    {
        if(!addNewStakeholder()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean addNewStakeholder(){
        //actions tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 5; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.actions_tab())){
            error = "Failed to wait for 'actions' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.actions_tab())){
            error = "Failed to click 'actions' tab.";
            return false;
        }        
        pause(2000);
        //actions add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.a_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.a_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }
        
        //Processflow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.pf())){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.pf())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow.");
        
        //Action description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_desc())){
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.Actions_desc(), testData.getData("Description"))){
            error = "Failed to enter '"+testData.getData("Description")+"' into Description textarea.";
            return false;
        }
        
        //Department Responsible
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_deptresp_dropdown())){
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Actions_deptresp_dropdown())){
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_deptresp_select(testData.getData("Department Responsible")))){
            error = "Failed to wait for '"+testData.getData("Department Responsible")+"' in Department Responsible dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Actions_deptresp_select(testData.getData("Department Responsible")))){
            error = "Failed to click '"+testData.getData("Department Responsible")+"' from Department Responsible dropdown.";
            return false;
        }
        
        //Responsible person
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_respper_dropdown())){
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Actions_respper_dropdown())){
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_respper_select(testData.getData("Responsible Person")))){
            error = "Failed to wait for '"+testData.getData("Responsible Person")+"' in Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Actions_respper_select(testData.getData("Responsible Person")))){
            error = "Failed to click '"+testData.getData("Responsible Person")+"' from Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_ddate())){
            error = "Failed to wait for Due Date textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.Actions_ddate(), endDate)){
            error = "Failed to enter '"+endDate+"' into due date textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Actions_save())){
            error = "Failed to wait for Save button.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Actions_save())){
            error = "Failed to click Save button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 4000)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully saved actions.");
        
        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
//        
        return true;
    }

}
