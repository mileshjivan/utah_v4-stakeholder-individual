/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.MailSlurper_PageObjects;
import microsoft.exchange.webservices.data.property.complex.availability.Suggestion;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To MailSlurper",
        createNewBrowserInstance = true
)
public class MailSlurperSignIn extends BaseClass {

    String error = "";

    public MailSlurperSignIn() {

    }

    public TestResult executeTest() {

        if (!NavigateToMailSlurperSignInPage()) {
            return narrator.testFailed("Failed to navigate to MailSlurper Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToIsomoterics()) {
            return narrator.testFailed("Failed to sign into the MailSlurper Home Page");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToMailSlurperSignInPage() {

        if (!SeleniumDriverInstance.navigateTo(MailSlurper_PageObjects.mailSlurper())) {
            error = "Failed to navigate to MailSlurper Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToIsomoterics() {

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("Username"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("Password"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn())) {
            error = "Failed to click sign in button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");
        pause(5000);
        return true;

    }

}
