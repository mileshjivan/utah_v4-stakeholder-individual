/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Create Work History - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR5_Create_Work_History_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR5_Create_Work_History_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest()
    {
        if(!createWorkHistory()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean createWorkHistory(){
         //right arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        //right arrow click
        for (int i = 0; i < 3; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
        
        //Work History tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_tab())){
            error = "Failed to wait for 'Work History' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_tab())){
            error = "Failed to click 'Work History' tab.";
            return false;
        }      
        narrator.stepPassedWithScreenShot("Successfully clicked 'Work History' tab.");
        //Work History add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }   
        narrator.stepPassedWithScreenShot("Successfully navigated to Work History page.");
        pause(2000);
        //Work History process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_pf())){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_pf())){
            error = "Failed to click 'Process Flow' button.";
            return false;
        }    
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' button.");
        //job profile dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.jp_dropdown())){
            error = "Failed to wait for job profile dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.jp_dropdown())){
            error = "Failed to click job profile dropdown.";
            return false;
        }      
        //job profile select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.jp_select(getData("Job Profile")))){
            error = "Failed to wait for '"+getData("Job Profile")+"' from Job Profile dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.jp_select(getData("Job Profile")))){
            error = "Failed to click '"+getData("Job Profile")+"' from Job Profile dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected 'Job Profile' record.");
        //seg dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.seg_dropdown())){
            error = "Failed to wait for SEG dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.seg_dropdown())){
            error = "Failed to click SEG dropdown.";
            return false;
        }      
        //seg select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.seg_select(getData("SEG")))){
            error = "Failed to wait for '"+getData("SEG")+"' from SEG dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.seg_select(getData("SEG")))){
            error = "Failed to click '"+getData("SEG")+"' from SEG dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected 'SEG' record.");
        //seg dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_sdate())){
            error = "Failed to wait for Start Date input area.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.wh_sdate(), startDate)){
            error = "Failed to click Start Date input area.";
            return false;
        }  
        narrator.stepPassedWithScreenShot("Successfully enetered start date.");
        //save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_save())){
            error = "Failed to wait for Save button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_save())){
            error = "Failed to click Save button.";
            return false;
        }  
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        
        return true;
    }

}
