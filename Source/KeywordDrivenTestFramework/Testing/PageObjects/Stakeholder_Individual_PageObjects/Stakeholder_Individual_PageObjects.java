/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Stakeholder_Individual_PageObjects extends BaseClass
{

    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }  
    
    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String navigate_Stakeholders()
    {
        return "//label[text()='Stakeholder Management']";
    }

    public static String navigate_StakeholderIndividual()
    {
        return "//label[text()='Stakeholder Individual']";
    }

    public static String search()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[1]";
    }

    public static String SI_add()
    {
        return "//div[@id='btnActAddNew']";
    }
//div[@id='btnActApplyFilter']

    public static String si_fname()
    {
        return "//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']/div/div/input";
    }

    public static String si_lname()
    {
        return "//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']/div/div/input";
    }

    public static String si_knownas()
    {
        return "//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']/div/div/input";
    }

    public static String si_title_dropdown()
    {
        return "//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//li";
    }

    public static String si_title(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_relationshipOwner_dropdown()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

     public static String si_relationshipOwner_dropdownValue(String text)
    {
        return "//a[text()='" + text + "']";
    }
            
    public static String si_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String StakeholderCategoriesSelectAll()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }
    
    public static String si_detailsTab()
    {
        return "//li[@id='tab_DA528E34-BBB5-4777-A826-D546DBBB3A69']";
    }        
            
    public static String si_designation()
    {
        return "//div[@id='control_DE0171C4-DDEA-47BD-A5D5-F4DF639EC9E2']/div/div/input";
    }

    public static String si_dob()
    {
        return "//div[@id='control_501DB50D-5488-43AF-91D3-B74D77CC09A3']//input";
    }

    public static String si_engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    
    public static String si_age()
    {
        return "//div[@id='control_1BC13F9E-47D7-436E-A5BB-97E88B8D86F5']/div/input";
    }

    public static String si_idnum()
    {
        return "//div[@id='control_5A680F16-7C29-49C2-9698-68C906C1F35F']/div/div/input";
    }

     public static String si_primaryContactNum()
    {
        return "//div[@id='control_E47C7BB6-4A97-41C7-9E5D-C3C6FDA1B25C']/div/div/input";
    }
     
      public static String si_StreetAddress()
    {
        return "//div[@id='control_A02F0A7C-C785-431C-8D0C-921A485C8C97']//textarea";
    }
      
     public static String si_PostalCode()
    {
        return "//div[@id='control_F8B6B6D6-F042-40C2-8A54-08D8745DB284']/div/div/input";
    }
     
    public static String si_Location()
    {
        return "//div[@id='control_B9F0F229-C6DB-440C-8E07-5EF10C11442E']//li";
    }
     
    public static String si_Location_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
   
     public static String si_TrainingManagementTab()
    {
        return "//li[@id='tab_098EB02D-6F88-4D36-9C78-3A50A1E3F20A']";
    }
    
     public static String si_HighestLevelOfEducation()
    {
        return "//div[@id='control_305523F4-2581-484F-B20F-983A40ACDE60']//li";
    }
     
    public static String si_HighestLevelOfEducation_select(String text)
    {
        return "//a[text()='" + text + "']";
    }
     public static String si_CurrentJobProfile()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']//li";
    }
     
     public static String si_CurrentJobProfile_select(String text)
    {
        return "//a[text()='" + text + "']";
    }
     
    
             
    public static String si_gender_dropdown()
    {
        return "//div[@id='control_BC0B3729-5DA7-493E-BA90-653F5C3E6151']//li";
    }

    public static String si_gender_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_nationality_dropdown()
    {
        return "//div[@id='control_447AC363-4DFF-4821-AB92-6695191EC822']";
    }

    public static String si_nationality_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String profile_tab()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[text()='Profile']";
    }

    public static String si_stakeholdercat_selectall()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }

    public static String si_stakeholdercat(String text)
    {
        return "(//a[text()='" + text + "']/i[1])[1]";
    }

    public static String si_appbu_selectall()
    {
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }

    public static String si_appbu_expand(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String si_appbu(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_impacttypes_selectall()
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }

    public static String impacttypes_selectall()
    {
        return "//div[@id='control_18DD1597-B800-4D1B-B063-A2E539BB3B8A']//b[@original-title='Select all']";
    }
    
    public static String contactEnquiryOrTopic()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']//li";
    }
    
    public static String contactEnquiryOrTopicValue(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
     public static String location()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']//li";
    }
    
    public static String locationValue(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }
    
    public static String si_impacttypes(String text)
    {
        return "//a[text()='" + text + "']/i[1]";
    }

    public static String si_locationmarker()
    {
        return "//div[@id='control_6A7B2112-2526-41F1-893D-2702E81144D7']//div[@class='icon location mark mapbox-icons']";
    }

    public static String location_tab()
    {
        return "//div[text()='Location']";
    }

    public static String si_location()
    {
        return "jozi3.PNG";
    }

    public static String si_save()
    {
        return "//div[@id='btnSave_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

//    public static String si_save()
//    {
//        return "(//div[text()='Save'])[2]";
//    }
    
    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox()
    {
        return "//div[@id='control_1F0A25D4-9905-4913-BC61-B1EED48CCCC6']//b[@original-title='Link to a document']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String si_savetocontinue()
    {
        return "//div[text()='Save supporting documents']";
    }

    public static String si_rightarrow()
    {
        return "//div[@id='control_38AB48FE-5C8D-401B-948C-9985EF810CED']//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String sik_rightarrow()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }

    public static String sik_leftarrow()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[1]";
    }
    
    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String stakeholerDetails_Tab()
    {
        return "//li[@id='tab_DA528E34-BBB5-4777-A826-D546DBBB3A69']";
    }

    public static String accessContactInfo_Checkbox()
    {
        return "(//div[@id='control_72B1FE8B-186F-4363-84AE-B844D349555E']//div)[2]";
    }

    public static String primaryContactNo()
    {
        return "(//div[@id='control_E47C7BB6-4A97-41C7-9E5D-C3C6FDA1B25C']//input)[1]";
    }

    public static String secondaryContactNo()
    {
        return "(//div[@id='control_A7725878-35E5-4DA9-9D78-18F770172373']//input)[1]";
    }

    public static String emailAddress()
    {
        return "(//div[@id='control_D6050013-0413-45E9-87EB-942CD2CF9BA7']//input)[1]";
    }

    public static String streetAddress()
    {
        return "(//div[@id='control_A02F0A7C-C785-431C-8D0C-921A485C8C97']//textarea)[1]";
    }

    public static String correspondence_Checkbox()
    {
        return "(//div[@id='control_DB350B2C-C01A-40C0-BD35-5BD957C9AE4D']//div)[2]";
    }

    public static String correspondenceAddress()
    {
        return "(//div[@id='control_3CCEEABE-61CA-41A4-A905-05C200CE97BA']//textarea)[1]";
    }

    public static String comments()
    {
        return "(//div[@id='control_9EAB7FD6-BA53-4DAC-8CC9-435444A0C6C4']//textarea)[1]";
    }

//    public static String entity_tab()
//    {
//        return "//div[text()='Entities']";
//    }

     public static String entity_tab()
    {
        return "//li[@id='tab_B82C2B7B-17B4-489B-A8EB-B3C93477CBBE']";
    }
    
    public static String entity_add()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Add']";
    }

    public static String entity_name_dropdown()
    {
        return "//div[@id='control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D']//li";
    }

    public static String entity_name_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String stakeholder_position()
    {
        return "//div[@id='control_DE361E8C-B66C-4402-B695-58413A1CA2BA']//li";
    }

    public static String stakeholderPosition_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }
    
            
    public static String entity_save()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Save']";
    }

    public static String se()
    {
        return "//label[text()='Stakeholder Entity']";
    }

    public static String se_name()
    {
        return "(//td[text()='Entity name']/..//input)[2]";
    }

    public static String se_search()
    {
        return "//div[text()='Search']";
    }

    public static String se_select(String text)
    {
        return "//div[text()='" + text + "']";
    }

    public static String member()
    {
        return "//div[text()='Members']";
    }

    public static String members_wait()
    {
        return "(//div[text()='Drag a column header and drop it here to group by that column']/..//tr)[2]";
    }

    public static String isoHome()
    {
        return "//div[@class='iso header item brand']";
    }

    public static String left()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[1]";
    }

    public static String engagementsTab()
    {
        return "//li[@id='tab_CF5F4EDE-1F5A-487F-9505-56097D794D34']";
    }

    public static String addEngagementsButton()
    {
        return "//div[@id='control_B7FD90BD-B7B7-45E8-9010-0F3DA43D264D']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

     public static String engagementTitle()
    {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
     
    public static String businessUnitTab()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//li";
    }

//    public static String businessUnit_SelectAll()
//    {
//        return "(//span[@class='select3-arrow']//b[@class='select3-all'])[3]";
//    }

    public static String businessUnit_SelectAll()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//b[@original-title='Select all']";
    }

    
    public static String businessUnitSelect(String text)
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String expandGlobalCompany()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }

    public static String expandSouthAfrica()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }

    public static String businessUnit_GlobalCompany()
    {
        return "//a[contains(text(),'Global Company')]";
    }

    public static String specificLocation()
    {
        return "(//div[@id='control_0501A692-460C-4DB5-9EAC-F15EE8934113']//input)[1]";
    }

    public static String projectLink()
    {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }

    public static String projectTab()
    {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }

    public static String anyProject(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String cn_entity()
    {
        return "//div[text()='Create a new entity']";
    }

    public static String closeBusinessUnit()
    {
        return "(//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//span[2]//b[1])";
    }

    //FR4
//    public static String relationship_tab()
//    {
//        return "//div[text()='Relationships']";
//    }

    public static String relationship_tab()
    {
        return "//li[@id='tab_80C001EC-AB1F-4DA8-9B60-D1195CBF4D3B']";
    }
    
    public static String r_add()
    {
        return "//div[@id='control_32F8198B-15EA-4936-8A36-E7D8F25335E3']//div[text()='Add']";
    }

    public static String s_name_dropdown()
    {
        return "//div[@id='control_5F245AC7-D951-4043-BC9F-75A1DDEBA4F8']//li";
    }

    public static String s_name_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String tor_dropdown()
    {
        return "//div[@id='control_F2DF7289-3CBA-4ED1-8937-8C347D349930']//li";
    }

    public static String tor_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String r_save()
    {
        return "//div[@id='control_32F8198B-15EA-4936-8A36-E7D8F25335E3']//div[text()='Save']";
    }

    public static String new_individual()
    {
        return "//div[text()='Create a new individual']";
    }

    public static String actions_tab()
    {
        return "//div[text()='Actions']";
    }

    public static String a_add()
    {
        return "//div[@id='control_9C28A74C-46A0-4CA4-9378-F196C193BD8A']//div[text()='Add']";
    }

    public static String pf()
    {
        return "//div[@id='btnProcessFlow_form_3FA76381-27A7-4E29-B2EF-9BE41116F069']";
    }

    public static String Actions_desc()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Actions_deptresp_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }

    public static String Actions_deptresp_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }

    public static String Actions_respper_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }

    public static String Actions_respper_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[3]";
    }

    public static String Actions_ddate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

//    public static String Actions_save()
//    {
//        return "(//div[text()='Save'])[4]";
//    }

     public static String Actions_save()
    {
        return "//div[@id='btnSave_form_3FA76381-27A7-4E29-B2EF-9BE41116F069']";
    }
     
    
    
    public static String Actions_save2()
    {
        return "(//div[text()='Save'])[6]";
    }

    public static String Actions_savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String TM_tab()
    {
        return "//div[text()='Training Management']";
    }

    public static String cjp_dropdown()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']";
    }

    public static String cjp_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

//    public static String wh_tab()
//    {
//        return "(//div[text()='Work History'])[1]";
//    }

     public static String wh_tab()
    {
        return "//li[@id='tab_95ADAE33-1DDF-4B19-9259-999F62D0DF62']";
    }
     
    
    public static String wh_record()
    {
        return "//div[@id='control_4CC82A9F-308E-4CE2-AC57-E0B26DF8F468']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr";
    }

    public static String wh_wait()
    {
        return "//div[text()='Job profile']";
    }

    public static String wh_add()
    {
        return "//div[@id='control_4CC82A9F-308E-4CE2-AC57-E0B26DF8F468']//div[text()='Add']";
    }

    public static String wh_pf()
    {
        return "//div[@id='btnProcessFlow_form_9AABEE75-5E60-48CD-AFD3-E42F607DC50D']";
    }

    public static String jp_dropdown()
    {
        return "//div[@id='control_98786101-EED5-435E-80FE-79C026BF43C6']//li";
    }

    public static String jp_select(String text)
    {
        return "(//a[text()='" + text + "'])[2]";
    }

    public static String seg_dropdown()
    {
        return "//div[@id='control_90A502E8-1540-4CCC-AD8B-A85F27F39444']//li";
    }

    public static String seg_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String wh_sdate()
    {
        return "//div[@id='control_53CAA00D-89E4-48DC-8D51-7651B7B78CB6']//input";
    }

    public static String wh_save()
    {
        return "//div[@id='btnSave_form_9AABEE75-5E60-48CD-AFD3-E42F607DC50D']";
    }

    public static String oh_tab()
    {
        return "//div[@id='divForms']//div[text()='Occupational Hygiene']";
    }

    public static String oh_record(String text)
    {
        return "//div[@id='control_11C9D3BC-24DE-47DB-92E6-D24DF3F2010F']//div[text()='" + text + "']";
    }

    public static String oh_wait()
    {
        return "//div[text()='Type of monitoring']";
    }

    public static String ShowMapLink()
    {
        return "//div[@id='control_6723362D-F9E2-457C-B125-13246382C3F7']/div[1]";
    }

    public static String engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }

    public static String communicationWithTab()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']";
    }

    public static String communicationWith_SelectAll()
    {
        return "//div[@id='control_45EA783F-D6C7-452F-B628-C3C91C7AB4B5']//b[@class='select3-all']";
    }

    public static String communicationUsers()
    {
        return "//a[contains(text(),'IsoMetrix Users')]";
    }

    public static String usersTab()
    {
        return "//div[@id='control_CD33B817-1DF7-4499-A055-2A70F29398FC']";
    }

    public static String userOption(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String stakeholdersTab()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']";
    }

    public static String stakeholdersTab2()
    {
        return "//div[text()='Stakeholder Details']";
    }

    public static String stakeholder_Array(String data)
    {
        return "(//a[text()='" + data + "']/i[1])";
    }

    public static String anyStakeholders(String text)
    {
        return "(//a[contains(text(),'" + text + "')]/i)[1]";
    }

    public static String anyStakeholders2(String text, int counter)
    {
        return "(//a[contains(text(),'" + text + "')]/i[1])[ " + counter + " ]";
    }

    public static String Stakeholders_SelectAll()
    {
        return "//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//b[@class='select3-all']";
    }

    public static String entitiesTab()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']";
    }

    public static String entities(String data)
    {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String expandInPerson()
    {
        return "(//a[contains(text(),'In-person engagements')]//..//i)[1]";
    }

    public static String expandOtherForms()
    {
        return "(//a[contains(text(),'Other forms of engagement')]//..//i)[1]";
    }

    public static String anyEngagementMethod(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String engagementMethodTab()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }

    public static String engagementPurposeTab()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']//li";
    }

    public static String anyEngagementPurpose(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

//    public static String engagementTitle()
//    {
//        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
//    }

    public static String engagementDescription()
    {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }

    public static String engagementPlan()
    {
        return "(//div[@id='control_F082EFDC-2D10-478A-AB94-BD4A3FB4FE4B']//textarea)[1]";
    }

    public static String managementApprovalActionCheckBox()
    {
        return "(//div[@id='control_523CF0A9-803B-44F2-8572-BED13C022A03']/div)[1]";
    }

    public static String stakeholderApprovalAction()
    {
        return "(//div[@id='control_D0FFF631-1DA5-4BD3-A1C8-75A7527E6A34']/div)[1]";
    }

    public static String responsiblePersonTab()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']";
    }

    public static String numberOfAttendees()
    {
        return "(//div[@id='control_3D6CBB44-8CCF-4C7F-9150-D5DDD630995E']//input)[1]";
    }

    public static String anyResponsiblePerson(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String entitiesCloseDropdown()
    {
        return "//div[@id='control_4C008128-D6F8-4D73-9AF1-FBED1B1C1029']//b[@class='select3-down drop_click']";
    }

    public static String Engagement_save()
    {
        return "(//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[contains(text(),'Save')])[3]";
    }

    public static String projectTitle()
    {
        return "IsoMetrix";
    }

    public static String searchTab()
    {
        return "(//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div[contains(text(),'Search')])[1]";
    }

    public static String searchButton()
    {
        return "(//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div[contains(text(),'Search')])[2]";
    }

    public static String engagementsGridView(String data)
    {
        return "//div[@id='control_D4CEBDAB-4E0D-4289-9F32-1B4D143FF4E1']//div//table//span[contains(text(),'" + data + "')]";
    }

    public static String Stakeholders_Close()
    {
        return "(//div[@id='control_79D83FF1-6A75-4E3C-9555-ADEF3FFBFA07']//span[2]//b[1])";
    }

    public static String stakeholderEntity_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2D4B7042-3E2E-495C-A77F-1642D10D5F1E']";
    }

    public static String getEngamentRecord()
    {
        return "//div[@class='recnum']//div[@class='record']";
    }

    public static String recordNo_Search()
    {
        return "(//div[@id='control_89186F28-56D9-423C-9FA4-8D688243B982']//td[text()='Record number']/..//input)[2]";
    }

    public static String getStakeholderIndividualRecord()
    {
        return "//div[@class='recnum']//div[@class='record']";
    }

    public static String si_RecordNo()
    {
        return "(//td[text()='Record number']/..//input)[2]";
    }

    public static String si_select(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String si_name()
    {
        return "(//td[text()='Full name']/..//input)[2]";
    }

    public static String si_status()
    {
        return "//div[@id='control_170F62D3-43C4-4548-A7C3-2E856B137AC8']//span[@class='select3-chosen']";
    }

    public static String si_statusSelect(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String si_businessunit(String data)
    {
        return "//a[text()='" + data + "']/i[1]";
    }

    public static String si_impacttype(String data)
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//a[text()='" + data + "']/i[1]";
    }

    public static String inspection_RecordSaved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String si_relationshipOwner()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//span[@class='select3-chosen']";
    }

    public static String si_si_relationshipOwnerSelect(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String categoryLabel()
    {
        return "//div[text()='Stakeholder categories']";
    }

    public static String designation()
    {
        return "//div[text()='Designation']";
    }

    public static String addressPanel()
    {
        return "//div[@id='control_763E5982-3E9C-4FAE-9D02-647DF0AB255C']//div[@class='c-pnl-heading']";
    }

    public static String postalCode()
    {
        return "//div[@id='control_F8B6B6D6-F042-40C2-8A54-08D8745DB284']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String southAfricaArrow()
    {
        return "//a[text()='South Africa']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String gautengArrow()
    {
        return "//a[text()='Gauteng']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String jhbArrow()
    {
        return "//a[text()='Johannesburg']/../i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectLocation(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String latitude()
    {
        return "//div[@id='control_44A782AF-225A-44F5-9434-F4E01DF62B38']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String longitude()
    {
        return "//div[@id='control_6E079358-9A1C-4480-B174-50B2D8D9097C']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String trainingManagementTab()
    {
        return "//div[text()='Training Management']";
    }

    public static String levelOfEducationDropdown()
    {
        return "//div[@id='control_305523F4-2581-484F-B20F-983A40ACDE60']//span[@class='select3-chosen']";
    }

    public static String levelOfEducationSelect(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String currentJobDropdown()
    {
        return "//div[@id='control_E9A39B73-DFAF-4168-B5EB-1E0C9F017650']//span[@class='select3-chosen']";
    }

    public static String currentJobSelect(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String doNotContactChkBox()
    {
        return "//div[@id='control_75AD2A23-263F-4AE2-9F25-2EB4289FABE3']/div[@class='c-chk']";
    }

    public static String alternateContact()
    {
        return "//div[@id='control_EF9ED37D-3A57-4C98-9B4C-706C651662D8']//span[@class='select3-chosen']";
    }

    public static String alternateContactSelect(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String relationShipToStakeholder()
    {
        return "//div[@id='control_72C247C0-E132-44CC-BD6B-1D0509E66942']//span[@class='select3-chosen']";
    }

    public static String relationShipToStakeholderSelect(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String homeButton()
    {
        return "//div[@class='iso header item brand']//div[text()='Solution Templates']";
    }

    public static String si_searchButton()
    {
        return "//div[@class='actionBar filter']//div[text()='Search']";
    }

    public static String selectRecord(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String contactNo()
    {
        return "//div[@id='control_53EA1874-F86E-4399-938E-327B1C909030']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String recordNoField()
    {
        return "(//input[@class='txt border'])[2]";
    }

    public static String socialStatusTab()
    {
        return "//span[text()='Social Status']";
    }

    public static String phoneLabel()
    {
        return "//div[text()='Phone number']";
    }

    public static String searchBtn()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String searchField()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[2]";
    }

    public static String selectRecorda(String data)
    {
        return "//span[text()='" + data + "']";
    }

    public static String entitiesTabs()
    {
        return "//div[text()='Entities']";
    }

    public static String entitiesAdd()
    {
        return "//div[@id='control_EE0F548B-BD30-4478-B62D-05A394FE49A3']//div[text()='Add']";
    }
    
    public static String entityNameDD()
    {
        return "//div[@id='control_9CC0D6E7-9FCE-48E5-B28D-C1BCDB97281D']//span[@class='select3-chosen']";
    }
    
    public static String entityNameSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }
    
    public static String stakeholderPositionDD()
    {
        return "//div[@id='control_DE361E8C-B66C-4402-B695-58413A1CA2BA']//span[@class='select3-chosen']";
    }
    
    public static String stakeholderPositionSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }
    
    public static String relatedSaveBtn()
    {
        return "//div[@id='btnSave_form_DF1172BD-686F-4776-A54F-2498D008B515']//div[text()='Save']";
    }

    public static String selectEntityRecord(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String selectOrderRecord(String data)
    {
       return "//div[text()='"+data+"']";
    }

    public static String employeesTab()
    {
        return "//div[text()='Employees']";
    }

    public static String selectRelatedRecord(String data)
    {
        return "//div[@id='control_196AE3D2-DDAF-4B03-BB99-6040D682878D']//div[text()='"+data+"']";
    }
    
    public static String searchFields()
    {
        return "(//div[@id='searchOptions']//input[@class='txt border'])[1]";
    }

    public static String addContractorBtn()
    {
        return "//div[text()='Add Contractor']";
    }

    public static String searchFieldIndividual()
    {
        return "(//div[@id='searchOptions']//td[@class='sel']//input[@class='txt border'])[1]";
    }

    public static String selectIndividualRecord(String data)
    {
        return "//div[text()='"+data+"']";
    }

    public static String vulnerabilityTab()
    {
        return "//div[text()='Vulnerability']";
    }

    public static String vulnerabilityConfirmed()
    {
        return "//div[@id='control_AEDC9FCF-6D3C-441F-AF04-32727FF4FB69']";
    }

    public static String vulnerabilityConfirmedSelect(String data)
    {
       return "//a[text()='"+data+"']";
    }

    public static String vulnerabilityCategorization()
    {
        return "//div[@id='control_248B60E1-72E9-469B-82CF-DF61210D6790']";
    }

    public static String vulnerabilityCategorizationSelect(String data)
    {
        return "//a[text()='"+data+"']";
    }

    public static String vulnerabilityType()
    {
        return "//div[@id='control_2A441998-4C0E-41D7-99CD-F523407F28F0']//b[@class='select3-all']";
    }

    public static String vulnerabilityDescription(String data)
    {
        return "//div[@id='control_FA88FC96-2078-4A7D-B988-CBDCC1AAE0BC']//textarea";
    }

    public static String location_Dropdown() {
        return "//div[@id='control_B9F0F229-C6DB-440C-8E07-5EF10C11442E']";
    }

    public static String step1_Registration() {
        return "//li[@id='tab_ECE1C32A-795E-42D7-8987-8720DBD32D6F']";
    }
    public static String failed(){
       return "(//div[@id='txtAlert'])[2]";
    }
//
//    public static String inspection_RecordSaved_popup() {
//        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
//    }
    public static String recordSaved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }
    public static String recordSaved_popup1() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[1]";
    }
    public static String recordNoChanges_Saved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record has no changes to save')])[1]";
    }
    
}
