/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Stakeholders - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Related_Stakeholders_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_Capture_Related_Stakeholders_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!addNewStakeholder()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean addNewStakeholder(){
        //left arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to wait for left arrow.";
                return false;
            }
        for (int i = 0; i < 5; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click left arrow.";
                return false;
            }
        }

        //relationships tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to wait for 'relationships' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to click 'relationships' tab.";
            return false;
        }        
        pause(2000);
        //relationships add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.r_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.r_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }
        //Stakeholder name dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.s_name_dropdown())){
            error = "Failed to wait for 'Stakeholder name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.s_name_dropdown())){
            error = "Failed to click 'Stakeholder name' dropdown.";
            return false;
        }      
        //Stakeholder name select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.s_name_select(getData("Stakeholder Name")))){
            error = "Failed to wait for '"+getData("Stakeholder Name")+"' from Stakeholder Name dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.s_name_select(getData("Stakeholder Name")))){
            error = "Failed to click '"+getData("Stakeholder Name")+"' from Stakeholder Name dropdown.";
            return false;
        }
        //Stakeholder Type of Relationship
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.tor_dropdown())){
            error = "Failed to wait for 'Type of Relationship' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.tor_dropdown())){
            error = "Failed to click 'Type of Relationship' dropdown.";
            return false;
        }      
        //Stakeholder name select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.tor_select(getData("Type of Relationship")))){
            error = "Failed to wait for '"+getData("Type of Relationship")+"' from Type of Relationship dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.tor_select(getData("Type of Relationship")))){
            error = "Failed to click '"+getData("Type of Relationship")+"' from Type of Relationship dropdown.";
            return false;
        }
        //relationships save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.r_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.r_save())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
