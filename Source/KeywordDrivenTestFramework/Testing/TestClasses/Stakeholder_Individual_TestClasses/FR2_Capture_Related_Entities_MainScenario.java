/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Entities - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Related_Entities_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR2_Capture_Related_Entities_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!addNewEntity()){
            return narrator.testFailed("Failed due - " + error);
        }
        
//        if (!navigateToStakeholderEntity())
//        {
//            return narrator.testFailed("Failed due - " + error);
//        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

//    public boolean navigateToStakeholderEntity(){
//        if(!SeleniumDriverInstance.switchToDefaultContent()){
//            error = "Failed to switch to default content.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.isoHome())){
//            error = "Failed to wait for 'ISOMETRIX' home button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.isoHome())){
//            error = "Failed to click on 'ISOMETRIX' home button.";
//            return false;
//        }
//        //switch to the iframe
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        //Navigate to Environmental Health & Safety
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
//            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
//            error = "Failed to click on 'Environmental, Health & Safety' tab.";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
//        
//        //Navigate to Stakeholders
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
//            error = "Failed to wait for 'Stakeholders' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
//            error = "Failed to click on 'Stakeholders' tab.";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
//        
//        //Navigate to Stakeholders Entity
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se())){
//            error = "Failed to wait for 'Stakeholders Entity' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se())){
//            error = "Failed to click on 'Stakeholders Entity' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");
//        
//        //Entity name
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_name())){
//            error = "Failed to wait for 'Entity name' input area.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.se_name(), getData("Entity Name"))){
//            error = "Failed to enter text '"+getData("Entity Name")+"' into 'Entity name' input area.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully entered 'Entity name' into search.");
//        
//        pause(2000);
//        //search button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_search())){
//            error = "Failed to wait for 'Search' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se_search())){
//            error = "Failed to click on 'Search' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked on 'Search' button.");
//        //record select
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_select(getData("Entity Name")))){
//            error = "Failed to wait for '"+getData("Entity Name")+"' record.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se_select(getData("Entity Name")))){
//            error = "Failed to click on '"+getData("Entity Name")+"' record.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked on '"+getData("Entity Name")+"' record.");
//        pause(2000);
//        
//        //members tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.member())){
//            error = "Failed to wait for 'Members' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.member())){
//            error = "Failed to click on 'Members' tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked on 'Members' tab.");
//        //members wait
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.members_wait())){
//            error = "Failed to wait for 'Members' record.";
//            return false;
//        }
//        pause(2000);
//        return true;
//    }
    
    //Enter data
    public boolean addNewEntity(){
        //left arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to wait for left arrow.";
                return false;
            }
        for (int i = 0; i < 8; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
        //entities tab
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_tab())){
            error = "Failed to wait for 'Entities' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_tab())){
            error = "Failed to click 'Entities' tab.";
            return false;
        }        
        pause(2000);
        //entities add
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_add())){
            error = "Failed to click 'Add' button.";
            return false;
        }
        //entity name dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_name_dropdown())){
            error = "Failed to wait for 'entity name' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_name_dropdown())){
            error = "Failed to click 'entity name' dropdown.";
            return false;
        }    
       
        //entity name select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_name_select(getData("Entity Name")))){
            error = "Failed to wait for '"+getData("Entity Name")+"' from Entity Name dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_name_select(getData("Entity Name")))){
            error = "Failed to click '"+getData("Entity Name")+"' from Entity Name dropdown.";
            return false;
        }
        
        //stakeholder position
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholder_position())){
            error = "Failed to wait for 'stakeholder position' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholder_position())){
            error = "Failed to click 'stakeholder position' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderPosition_select(getData("stakeholder position")), 2000)){
            error = "Failed to wait for '"+getData("stakeholder position")+"' from stakeholder position dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderPosition_select(getData("stakeholder position")))){
            error = "Failed to click '"+getData("stakeholder position")+"' from stakeholder position dropdown.";
            return false;
        }
        
        //entities save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_save())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
