/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Create Work History - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Create_Work_History_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR5_Create_Work_History_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!createWorkHistory()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean createWorkHistory(){
//        //Training Management tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.TM_tab())){
//            error = "Failed to wait for 'Training Management' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.TM_tab())){
//            error = "Failed to click 'Training Management' tab.";
//            return false;
//        }        
//        narrator.stepPassedWithScreenShot("Successfully clicked 'Training Management' tab.");
//        pause(2000);
//        //Current job profile dropdown
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.cjp_dropdown())){
//            error = "Failed to wait for 'Current job profile' dropdown.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.cjp_dropdown())){
//            error = "Failed to click 'Current job profile' dropdown.";
//            return false;
//        }      
//        //Current job profile select
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.cjp_select(getData("Current job profile")))){
//            error = "Failed to wait for '"+getData("Current job profile")+"' from Current job profile dropdown.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.cjp_select(getData("Current job profile")))){
//            error = "Failed to click '"+getData("Current job profile")+"' from Current job profile dropdown.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully selected 'Current job profile'.");
//        //save
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
//            error = "Failed to wait for 'Save' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
//            error = "Failed to click 'Save' button.";
//            return false;
//        }   
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
//      
//        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        
        //right arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        //right arrow click
        for (int i = 0; i < 3; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
        //Work History tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_tab())){
            error = "Failed to wait for 'Work History' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_tab())){
            error = "Failed to click 'Work History' tab.";
            return false;
        }      
        narrator.stepPassedWithScreenShot("Successfully clicked 'Work History' tab.");
        //Work History record
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_record())){
            error = "Failed to wait for 'Work History' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.wh_record())){
            error = "Failed to click 'Work History' record.";
            return false;
        }   
        //relationships save
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.wh_wait())){
            error = "Failed to wait for 'Work History' page.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Work History' record.");
        
        return true;
    }

}
