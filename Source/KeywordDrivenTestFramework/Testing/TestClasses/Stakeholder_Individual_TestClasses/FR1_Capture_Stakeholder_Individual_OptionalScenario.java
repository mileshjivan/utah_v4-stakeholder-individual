/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Stakeholder Individual - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Stakeholder_Individual_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR1_Capture_Stakeholder_Individual_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!createNewIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean navigateToStakeholderIndividual(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Individual
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholders Individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholders Individual' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.SI_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.SI_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean createNewIndividual(){
        //Switch to new tab
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_fname())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_fname(), testData.getData("First name"))){
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        } 
        //Stakeholder Individual last name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_lname())){
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_lname(), testData.getData("Last name"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        //Stakeholder Individual known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_knownas())){
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        } 
        //Stakeholder Individual title dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to select '"+testData.getData("Stakeholder title")+"' from 'Stakeholder title' options.";
            return false;
        }
        //Stakeholder Individual designation
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_designation())){
            error = "Failed to wait for 'Stakeholder Individual Designation' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_designation(), testData.getData("Designation"))){
            error = "Failed to click 'Stakeholder Individual Designation' input field.";
            return false;
        } 
        //Stakeholder Individual DoB
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_dob())){
            error = "Failed to wait for 'Stakeholder Individual DoB' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_dob(), testData.getData("DoB"))){
            error = "Failed to click 'Stakeholder Individual DoB' input field.";
            return false;
        } 
        //Stakeholder Individual age
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_idnum())){
            error = "Failed to wait for 'Stakeholder Individual ID num' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_idnum(), testData.getData("ID num"))){
            error = "Failed to click 'Stakeholder Individual ID num' input field.";
            return false;
        } 
        //Stakeholder Individual gender dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_gender_dropdown())){
            error = "Failed to wait for gender dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_gender_dropdown())){
            error = "Failed to click gender dropdown.";
            return false;
        }
        //Stakeholder Individual gender select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_gender_select(testData.getData("Gender")))){
            error = "Failed to wait for gender dropdown option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_gender_select(testData.getData("Gender")))){
            error = "Failed to click gender dropdown option.";
            return false;
        }
        //Stakeholder Individual nationality dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_nationality_dropdown())){
            error = "Failed to wait for gender dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_nationality_dropdown())){
            error = "Failed to click gender dropdown.";
            return false;
        }
        //Stakeholder Individual nationality select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_nationality_select(testData.getData("Nationality")))){
            error = "Failed to wait for Nationality dropdown option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_nationality_select(testData.getData("Nationality")))){
            error = "Failed to click Nationality dropdown option.";
            return false;
        }
        //Profile tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.profile_tab())){
            error = "Failed to wait for Profile tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.profile_tab())){
            error = "Failed to click Profile tab.";
            return false;
        }
        //Stakeholder Individual categorie 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        //Stakeholder Individual categorie 2
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 2")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 2")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        //Stakeholder Individual Business Unit
        if(!testData.getData("Business Unit").equals("Global Company")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            if(!testData.getData("Business Unit").equals("South Africa")){
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to wait to expand 'South Africa'.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to expand 'South Africa'.";
                    return false;
                }
                if(!testData.getData("Business Unit").equals("Victory Site")){
                    error = "Failed to find Business Unit";
                    return false;
                }else
                {
                    if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                }
            }else
            {
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
            }
        }else
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
        }
        //Stakeholder Individual Impact Types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_impacttypes_selectall())){
            error = "Failed to wait for Stakeholder Individual Impact Types.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_impacttypes_selectall())){
            error = "Failed to click Stakeholder Individual Impact Types select all.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Profile details entered.");
        //Stakeholder Individual Locations tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.location_tab())){
            error = "Failed to wait for Location tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.location_tab())){
            error = "Failed to click Location tab.";
            return false;
        }
        //Stakeholder Individual Location select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_locationmarker())){
            error = "Failed to wait for Location Marker.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_locationmarker())){
            error = "Failed to click Location Marker.";
            return false;
        }
        //Sikuli 
        String pathofImages = System.getProperty("user.dir") + "\\images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);
        
        if (!sikuliDriverUtility.MouseClickElement(Stakeholder_Individual_PageObjects.si_location())) {
            error = "Failed to click location on map.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Location Selected.");
        
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        //Navigate to Supporting Documents
        for (int i = 0; i < 10; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click on right arrow.";
                return false;
            }
        }
        //Supporting Documents tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.supporting_tab())){
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.supporting_tab())){
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        //Add support document - Hyperlink
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.linkbox())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.linkbox())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.urlTitle(), getData("URL Title"))){
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        return true;
    }

}
