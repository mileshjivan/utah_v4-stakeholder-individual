/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Entities - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Related_Entities_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR2_Capture_Related_Entities_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!createNewEntity()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean createNewEntity(){
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //left arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to wait for left arrow.";
                return false;
            }
        for (int i = 0; i < 8; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
        //entities tab
         pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_tab())){
            error = "Failed to wait for 'Entities' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_tab())){
            error = "Failed to click 'Entities' tab.";
            return false;
        }        
        pause(2000);
        //entities new
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.cn_entity())){
            error = "Failed to wait for 'Create new entity' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.cn_entity())){
            error = "Failed to click 'Create new entity' button.";
            return false;
        }
        
        pause(15000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
         //Process flow 
         pause(10000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_processflow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
          //Entity Type dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown(),5000)){
            error = "Failed to wait for 'Entity Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdown())){
            error = "Failed to click 'Entity Type' dropdown.";
            return false;
        }
        
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType"))))
        {
            error = "Failed to locate '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_EntityType_dropdownValue(testData.getData("EntityType"))))
        {
            error = "Failed to enter '"+testData.getData("Entity Type")+"' into 'Entity Type' input.";
            return false;
        }
        
        //Entity name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_name())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_name(), testData.getData("Entity Name"))){
            error = "Failed to enter '"+testData.getData("Entity Name")+"' into 'Entity name' input.";
            return false;
        }
        //Industry dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to wait for 'Industry' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_dropdown())){
            error = "Failed to click 'Industry' dropdown.";
            return false;
        }
        //Industry select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry")))){
            error = "Failed to wait for 'Industry' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_industry_select(testData.getData("Industry")))){
            error = "Failed to select '"+testData.getData("Industry")+"' from 'Industry' dropdown options.";
            return false;
        }
        //Entity description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_desc())){
            error = "Failed to wait for 'Entity name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Entity_PageObjects.SE_desc(), testData.getData("Entity description"))){
            error = "Failed to enter '"+testData.getData("Entity description")+"' into 'Entity description' textarea.";
            return false;
        }
        //Relationship owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        //Relationship owner select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' dropdown options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_owner_select(testData.getData("Relationship owner")))){
            error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' dropdown options.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Entity details completed");
        
        //Stakeholder categories
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories")))){
            error = "Failed to wait for '"+testData.getData("Categories")+"' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_categories(testData.getData("Categories")))){
            error = "Failed to click '"+testData.getData("Categories")+"' option.";
            return false;
        }
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to wait for 'Business unit' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_business_unit_selectall())){
            error = "Failed to click 'Business unit' select all.";
            return false;
        }
        //Impact types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to wait for 'Impact type' select all.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_impact_types_selectall())){
            error = "Failed to click 'Impact type' select all.";
            return false;
        }
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Entity_PageObjects.SE_savetocontinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
         //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          
//        //switch to the iframe
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
        //entities add
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_add())){
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_add())){
//            error = "Failed to click 'Add' button.";
//            return false;
//        }
//        //entity name dropdown
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_name_dropdown())){
//            error = "Failed to wait for 'entity name' dropdown.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_name_dropdown())){
//            error = "Failed to click 'entity name' dropdown.";
//            return false;
//        }      
//        //entity name select
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_name_select(getData("Entity Name")))){
//            error = "Failed to wait for '"+getData("Entity Name")+"' from Entity Name dropdown.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_name_select(getData("Entity Name")))){
//            error = "Failed to click '"+getData("Entity Name")+"' from Entity Name dropdown.";
//            return false;
//        }
//        //stakeholder position
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholder_position())){
//            error = "Failed to wait for 'stakeholder position' input area.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.stakeholder_position(), getData("Stakeholder Position"))){
//            error = "Failed to enter text '"+getData("Stakeholder Position")+"' into 'stakeholder position' input area.";
//            return false;
//        }
//        
//        //entities save
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entity_save())){
//            error = "Failed to wait for 'Save' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entity_save())){
//            error = "Failed to click 'Save' button.";
//            return false;
//        }
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
//      
//        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        return true;
    }

}
