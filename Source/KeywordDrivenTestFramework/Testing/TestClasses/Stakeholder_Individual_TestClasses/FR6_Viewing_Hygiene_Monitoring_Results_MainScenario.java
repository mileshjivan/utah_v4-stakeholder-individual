/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Viewing Hygiene Monitoring Results - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_Viewing_Hygiene_Monitoring_Results_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR6_Viewing_Hygiene_Monitoring_Results_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!occupationalHygiene()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean navigateToStakeholderIndividual(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Individual
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholders Individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholders Individual' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");
        
        //Search button
        pause(15000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_search(), 10000)){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se_search())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //existing record
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_select(getData("Stakeholder Individual")), 10000)){
            error = "Failed to wait for 'Stakeholder Individual' existing record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se_select(getData("Stakeholder Individual")))){
            error = "Failed to click on 'Stakeholder Individual' existing record.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Stakeholder Individual' existing record.");
        return true;
    }
    
    public boolean occupationalHygiene(){
        //Process flow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Processflow' button.");
        //Occupational Hygiene tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 8; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.oh_tab())){
            error = "Failed to wait for 'Occupational Hygiene' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.oh_tab())){
            error = "Failed to click 'Occupational Hygiene' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Occupational Hygiene' tab.");
        //Occupational Hygiene record
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.oh_record(testData.getData("Occupational Hygiene Person")))){
            error = "Failed to wait for existing record in 'Hygiene Monitoring Results'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully loaded existing record in 'Hygiene Monitoring Results' tab.");
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.oh_record(testData.getData("Occupational Hygiene Person")))){
            error = "Failed to select existing record from 'Hygiene Monitoring Results'.";
            return false;
        }
        //Occupational Hygiene wait
        pause(15000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.oh_wait())){
            error = "Failed to wait for 'Hygiene Monitoring Stressors' page.";
            return false;
        }  
        
                 
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Hygiene Monitoring Stressors' page.");
       
        return true;
    }

}
