/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Update stakeholder details Alt2",
        createNewBrowserInstance = false
)

public class UC_STA_01_02_UpdateStakeholderDetails_AlternateScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public UC_STA_01_02_UpdateStakeholderDetails_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if (!NavigateToStakeholderDetails())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!UpdateStakeholderIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }

    public boolean NavigateToStakeholderDetails(){
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
            error = "Failed to wait for left arrow.";
            return false;
        }
        //Navigate to Stakeholder Details Tab
        for (int i = 0; i < 11; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
        
        //Navigate to Stakeholder Details Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to wait for 'Stakeholder Details' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholerDetails_Tab())){
            error = "Failed to click on 'Stakeholder Details' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholder Details' tab.");
        
        return true;
    }
    
    public boolean UpdateStakeholderIndividual(){
        if(getData("Access Contact").equalsIgnoreCase("TRUE")){
            //Access contact Information
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.accessContactInfo_Checkbox())){
                error = "Failed to wait for 'Access contact Information' checkbox.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.accessContactInfo_Checkbox())){
                error = "Failed to click on 'Access contact Information' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 'Access contact Information' checkbox.");

            //Contact Information: //
            //Primary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.primaryContactNo())){
                error = "Failed to wait for 'Primary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.primaryContactNo(), getData("Primary Contact Number"))){
                error = "Failed to enter '" + getData("Primary Contact Number") + "' into 'Primary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Primary Contact Number : '" + getData("Primary Contact Number") + "'.");

            //Secondary contact number
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo())){
                error = "Failed to wait for 'Secondary contact number' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.secondaryContactNo(), getData("Secondary Contact Number"))){
                error = "Failed to enter '" + getData("Secondary Contact Number") + "' into 'Secondary contact number' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Secondary Contact Number : '" + getData("Secondary Contact Number") + "'.");

            //Email address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.emailAddress())){
                error = "Failed to wait for 'Email address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.emailAddress(), getData("Email address"))){
                error = "Failed to enter '" + getData("Email address") + "' into 'Email address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Email address : '" + getData("Email address") + "'.");

            //Street address
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.streetAddress())){
                error = "Failed to wait for 'Street address' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.streetAddress(), getData("Street address"))){
                error = "Failed to enter '" + getData("Street address") + "' into 'Street address' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Street address : '" + getData("Street address") + "'.");

            //Is the correspondence address different to the street address?
            if(getData("Correspondence Address").equalsIgnoreCase("TRUE")){
                //correspondence address checkbox
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondence_Checkbox())){
                    error = "Failed to wait for 'correspondence address' checkbox.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.correspondence_Checkbox())){
                    error = "Failed to click on 'correspondence address' checkbox.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked the 'correspondence address' checkbox.");

                //Correspondence address textarea
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress())){
                    error = "Failed to wait for 'correspondence address' input field.";
                    return false;
                }
                if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.correspondenceAddress(), getData("correspondence address"))){
                    error = "Failed to enter '" + getData("correspondence address") + "' into 'correspondence address' input field.";
                    return false;
                } 
                narrator.stepPassedWithScreenShot("correspondence address : '" + getData("correspondence address") + "'.");
            }

            //Comments
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.comments())){
                error = "Failed to wait for 'comments' input field.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.comments(), getData("comments"))){
                error = "Failed to enter '" + getData("comments") + "' into 'comments' input field.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Comments : '" + getData("comments") + "'.");
        }
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
