/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author MJivan
 */
public class MailSlurper_PageObjects {
    public static String mailSlurper(){
        return "http://qa01.isometrix.net:8090/";
    }

    public static String Username() {
        return "//input[@id='userName']";
    }

    public static String Password() {
        return "//input[@id='password']";
    }

    public static String LoginBtn() {
        return "//button[@id='btnSubmit']";
    }

}
