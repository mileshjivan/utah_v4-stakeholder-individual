/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
//import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "FR3 Add_View Engagements - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR3_Add_View_Engagements_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR3_Add_View_Engagements_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if(!navigateToStakeholderIndividual()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!navigateToEngagements()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!EngagementWindow()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Engagement - record: #" + getRecordId());
    }

    public boolean navigateToStakeholderIndividual(){
        if(!SeleniumDriverInstance.switchToDefaultContent()){
            error = "Failed to switch to default content.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.isoHome())){
            error = "Failed to wait for 'ISOMETRIX' home button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.isoHome())){
            error = "Failed to click on 'ISOMETRIX' home button.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_Stakeholders())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to wait for 'Stakeholders Individual' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.navigate_StakeholderIndividual())){
            error = "Failed to click on 'Stakeholders Individual' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");
        
        //Stakeholder Individual Record Number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_RecordNo())){
            error = "Failed to wait for 'Record Number' input area.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_RecordNo(), getStakeholderRecordId())){
            error = "Failed to enter text '" + getStakeholderRecordId() + "' into 'Record Number' input area.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered 'Entity name' into search.");
        
        //search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.se_search())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.se_search())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'Search' button.");
        //record select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_select(getStakeholderRecordId()))){
            error = "Failed to wait for '" + getStakeholderRecordId() + "' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_select(getStakeholderRecordId()))){
            error = "Failed to click on '" + getStakeholderRecordId() + "' record.";
            return false;
        } 
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked on '" + getStakeholderRecordId() + "' record.");
       
        return true;
    }
    
    public boolean navigateToEngagements(){
        pause(5000);
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Left Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
            error = "Failed to wait for 'Left' Arrow.";
            return false;
        }
        for (int i = 0; i < 6; i++) {
            if(!SeleniumDriverInstance.doubleClickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click on 'Left' Arrow.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Left' Arrow.");
        
        //Navigate to Engaments Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to wait for 'Engagements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsTab())){
            error = "Failed to click on 'Engagements' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' tab.");
        
        return true;
    }
    
    public boolean enterDetails(){
        //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
         
        //Add an engagement button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.addEngagementsButton())){
            error = "Failed to wait for 'Add Engagements' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.addEngagementsButton())){
            error = "Failed to click on 'Add Engagements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add Engagements' button.");
        
        //switch to the new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch on the 'Engagements' window.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched to the 'Engagements' window..");
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.processFlow())){
            error = "Failed to wait for 'Engagement' Process Flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.processFlow())){
            error = "Failed to click on 'Engagement' Process Flow.";
            return false;
        }        
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement' Process Flow.");        
        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.businessUnitTab())){
            error = "Failed to wait for 'Business Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.businessUnitTab())){
            error = "Failed to click on 'Business Unit' tab.";
            return false;
        }
        //Expand Global Company
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        //Expand South Africa
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        
        if (getData("Select All").equalsIgnoreCase("YES")){
            //Select all 
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.businessUnit_SelectAll())){
                error = "Failed to wait for 'Business Unit' tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.businessUnit_SelectAll())){
                error = "Failed to click on 'Select All' Business Unit.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Select All' Business Unit."); 
        }else{
            //Victory Site
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.businessUnitSelect(getData("Business Unit")))){
                error = "Failed to wait for Business Unit option: '" + getData("Business Unit") + "'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.businessUnitSelect(getData("Business Unit")))){
                error = "Failed to select Business Unit option: '" + getData("Business Unit") + "'.";
                return false;
            }
            //Close Businuss Tab //remember to change
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.closeBusinessUnit())){
                error = "Failed to wait for 'Business unit' close tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.closeBusinessUnit())){
                error = "Failed to click on 'Business unit' close tab.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        }
         pause(2000);
        //Specific location
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.specificLocation())){
            error = "Failed to wait for 'Specific location' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.specificLocation(), getData("Specific Location"))){
            error = "Failed to enter '" + getData("Specific Location") + "' into 'Specific location' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Specific Location : '" + getData("Specific Location") + "' .");
        
        //Project Link checkbox
        if (getData("Project Link").equalsIgnoreCase("YES")) {
            //Check project link Checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.projectLink())) {
                error = "Failed to wait for 'Project link' check box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.projectLink())) {
                error = "Failed to click on 'Project link' check box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Project Link successfully checked.");

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.projectTab())) {
                error = "Failed to wait for 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.projectTab())) {
                error = "Failed to click on 'Project' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.anyProject(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' list.";
                return false;
            }
            SeleniumDriverInstance.pause(2000);
            narrator.stepPassedWithScreenShot("Project : '" + getData("Project") + "'.");
        }
        
        //Show map checkbox
         if (getData("Show map").equalsIgnoreCase("YES")) {
             if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.ShowMapLink())) {
                error = "Failed to wait for 'Show map' check box.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.ShowMapLink())) {
                error = "Failed to click on 'Show map' check box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Show map successfully checked.");
         }
         
        //Engagement date
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementDate())){
            error = "Failed to wait for 'Engagement date' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementDate(), startDate)){
            error = "Failed to enter '" + startDate + "' into 'Engagement date' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement date : '" + startDate + "'.");
         
//        //Communication with
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.communicationWithTab())){
//            error = "Failed to wait for 'Communication with' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.communicationUsers())){
//            error = "Failed to wait for 'Communication with' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.communicationWith_SelectAll())){
//            error = "Failed to click on 'Select All' Communication with.";
//            return false;
//        }
//        
//        //Users
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.usersTab())){
//            error = "Failed to wait for 'Users' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.usersTab())){
//            error = "Failed to click on 'Users' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.userOption(getData("Users")))){
//            error = "Failed to wait for '" + getData("Users") + "' user.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.userOption(getData("Users")))){
//            error = "Failed to click on '" + getData("Users") + "' user.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Users : '" + getData("Users")+ "'.");
        
        //stackeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholdersTab())){
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholdersTab())){
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        
        if(getData("Select Stackeholders").equalsIgnoreCase("NO")){
            //Select *
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Stakeholders_SelectAll())){
                error = "Failed to wait for 'Select All' Stakeholders.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Stakeholders_SelectAll())){
                error = "Failed to click on 'Select All' Stakeholders..";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked all.");
        }else{
            //Select any
            List<WebElement> stakeholders = SeleniumDriverInstance.Driver.findElements(By.xpath(Stakeholder_Individual_PageObjects.stakeholder_Array(getData("Stakeholder Name"))));
            
            int counter = stakeholders.size();
            
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.anyStakeholders2(getData("Stakeholder Name"), counter))){
                error = "Failed to wait for '" + getData("Stakeholder Name") + "' field.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.anyStakeholders2(getData("Stakeholder Name"), counter))){
                error = "Failed to wait for '" + getData("Stakeholder Name") + "' field.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Stakeholders_Close())){
                error = "Failed to wait for 'Close' Stakeholders.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Stakeholders_Close())){
                error = "Failed to click on 'Close' Stakeholders..";
                return false;
            }
            narrator.stepPassedWithScreenShot("Stakeholder Name : '" + getData("Stakeholder Name") + "'.");
        }
        pause(2000);
        //Entities
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entitiesTab())){
            error = "Failed to wait for 'Enties' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entitiesTab())){
            error = "Failed to click on 'Enties' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.entities(getData("Entity name")))){
            error = "Failed to wait for '" + getData("Entity name") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entities(getData("Entity name")))){
            error = "Failed to click on '" + getData("Entity name") + "' tab.";
            return false;
        }
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.entitiesCloseDropdown())){
            error = "Failed to click on 'Enties' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entity Name : '" + getData("Entity name") + "'.");
        
        
        //Engagement method
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementMethodTab())){
            error = "Failed to wait for 'Engagement method' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementMethodTab())){
            error = "Failed to click on 'Engagement method' tab.";
            return false;
        }
        //Expand In-Person engagements
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.expandInPerson())){
            error = "Failed to wait for 'In-person engagements' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.expandInPerson())){
            error = "Failed to click on 'In-person engagements' option.";
            return false;
        }
        //Any anyEngagement
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to wait for '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.anyEngagementMethod(getData("Engagement Method")))){
            error = "Failed to click on '" + getData("Engagement Method") + "' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Method : '" + getData("Engagement Method") + "'.");
        
        //Engagement purpose
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementPurposeTab())){
            error = "Failed to wait for 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementPurposeTab())){
            error = "Failed to click on 'Engagement Purpose' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to wait for '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.anyEngagementPurpose(getData("Engagement Purpose")))){
            error = "Failed to click on '" + getData("Engagement Purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Purpose : '" + getData("Engagement Purpose") + "'.");
        
        //Engagement title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementTitle())){
            error = "Failed to wait for 'Engagement Title' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementTitle(), getData("Engagement Title"))){
            error = "Failed to '" + getData("Engagement Title") + "' into Engagement Title field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Title : '" + getData("Engagement Title") + "'.");
        
        //Engagement description
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementDescription())){
            error = "Failed to wait for 'Engagement Description' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementDescription(), getData("Engagement Description"))){
            error = "Failed to '" + getData("Engagement Description") + "' into Engagement Description field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement description : '" + getData("Engagement Description") + "'.");
        
        //Engagement Plan
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementPlan())){
            error = "Failed to wait for 'Engagement Plan' field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.engagementPlan(), getData("Engagement Plan"))){
            error = "Failed to '" + getData("Engagement Plan") + "' into Engagement Plan field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Plan : '" + getData("Engagement Plan") + "'.");
        
        if(getData("Management Approval").equalsIgnoreCase("YES")){   
            //ManagementApprovalAction
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.managementApprovalActionCheckBox())){
                error = "Failed to wait for 'Management Approval Action' checkbox.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.managementApprovalActionCheckBox())){
                error = "Failed to click on 'Management Approval Action' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked 'Management Approval Action' checkbox.");

            //StakeholderApprovalAction
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderApprovalAction())){
                error = "Failed to wait for 'Stakeholder Approval Action' checkbox.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderApprovalAction())){
                error = "Failed to click on 'Stakeholder Approval Action' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked 'Stakeholder Approval Action' checkbox.");
        }
        
        //Number of attendences
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.numberOfAttendees())){
            error = "Failed to wait for 'Number of attendees' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpathUsingActions(Stakeholder_Individual_PageObjects.numberOfAttendees(), getData("Number of attendees"))){
            error = "Failed to enter '" + getData("Number of attendees") + "' into 'Number of attendees' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Number of attendees : '" + getData("Number of attendees") + "'.");
        
       //Responsible person
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.responsiblePersonTab())){
           error = "Failed to wait for 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.responsiblePersonTab())){
           error = "Failed to click on 'Responsible Person' tab.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.anyResponsiblePerson(getData("Responsible Person")))){
           error = "Failed to click on '" + getData("Responsible Person") + "' option.";
           return false;
       }
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.Engagement_save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.Engagement_save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully 'Save' Record: #" + getRecordId());
        
        //Get the Record No
        String[] engagementRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getEngamentRecord()).split("#");
        setRecordId(engagementRecord[1]);
        
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        
        //Switch to a new tab or window
        if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver, Stakeholder_Individual_PageObjects.projectTitle())){
            error = "Failed to switch to 'IsoMetrix' window.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully switched to 'IsoMetrix' window.");
        
        return true;
    }
    
    public boolean EngagementWindow(){
        //Search Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchTab())){
            error = "Failed to wait for 'Search' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchTab())){
            error = "Failed to click on 'Search' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchButton())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchButton())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsGridView(getRecordId()))){
            error = "Failed to wait for 'Engagements' grid view.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully 'Search' button clicked.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsGridView(getRecordId()))){
            error = "Failed to wait for 'Engagements' grid view.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsGridView(getRecordId()))){
            error = "Failed to wait for 'Engagements' grid view.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully retrieved record: #" + getRecordId());
        pause(7000);
        
        return true;
    }
    
    
    
//    public boolean EngagementWindow(){
//        //Process flow
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.stakeholderEntity_ProcessFlow())){
//            error = "Failed to wait for 'Stakeholder Entity' Process Flow.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.stakeholderEntity_ProcessFlow())){
//            error = "Failed to click on 'Stakeholder Entity' Process Flow.";
//            return false;
//        }        
//        narrator.stepPassedWithScreenShot("Successfully click 'Engagement' Process Flow.");
//        
//        //Search Tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchTab())){
//            error = "Failed to wait for 'Search' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchTab())){
//            error = "Failed to click on 'Search' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordNo_Search())){
//            error = "Failed to wait for '" + getRecordId() + "' input.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.recordNo_Search(), getRecordId())){
//            error = "Failed to wait for '" + getRecordId() + "' input.";
//            return false;
//        }
//        pause(2000);
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.searchButton())){
//            error = "Failed to wait for 'Search' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.searchButton())){
//            error = "Failed to click on 'Search' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.engagementsGridView(getRecordId()))){
//            error = "Failed to wait for 'Engagements' grid view.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.engagementsGridView(getRecordId()))){
//            error = "Failed to wait for 'Engagements' grid view.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully 'Search' button clicked.");
//        narrator.stepPassedWithScreenShot("Successfully retrieved record: #" + getRecordId());        
//        pause(2000);
//        
//        return true;
//    }
   

}
