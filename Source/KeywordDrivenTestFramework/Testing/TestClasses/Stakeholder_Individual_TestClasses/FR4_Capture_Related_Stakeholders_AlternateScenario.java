/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Stakeholder_Individual_TestClasses;



import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Stakeholder_Individual_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.logging.LogType;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Related Stakeholders - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Related_Stakeholders_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR4_Capture_Related_Stakeholders_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");   
    }

    public TestResult executeTest()
    {
        if(!addNewStakeholder()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Stakeholder Individual ");
    }
    
    //Enter data
    public boolean addNewStakeholder(){
        //left arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to wait for left arrow.";
                return false;
            }
        for (int i = 0; i < 5; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.left())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
        //relationships tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to wait for 'relationships' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.relationship_tab())){
            error = "Failed to click 'relationships' tab.";
            return false;
        }        
        pause(2000);
        //relationships new
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.new_individual())){
            error = "Failed to wait for 'Create a new individual' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.new_individual())){
            error = "Failed to click 'Create a new individual' button.";
            return false;
        }
        
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
         pause(15000);
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
        
//        FR1_Capture_Stakeholder_Individual_MainScenario FR1 = new FR1_Capture_Stakeholder_Individual_MainScenario();
//        FR1.createNewIndividual();
//        
//        pause(2000);
//        SeleniumDriverInstance.switchToDefaultContent();
//        SeleniumDriverInstance.Driver.close();
//        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        
  if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_processflow(), 5000)){
            error = "Failed to wait for Process flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_processflow())){
            error = "Failed to click Process flow.";
            return false;
        }
        //Stakeholder Individual first name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_fname())){
            error = "Failed to wait for 'Stakeholder Individual first name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_fname(), testData.getData("First name"))){
            error = "Failed to click 'Stakeholder Individual first name' input field.";
            return false;
        } 
        //Stakeholder Individual last name
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_lname())){
            error = "Failed to wait for 'Stakeholder Individual last name' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_lname(), testData.getData("Last name"))){
            error = "Failed to click 'Stakeholder Individual last name' input field.";
            return false;
        } 
        //Stakeholder Individual known as
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_knownas())){
            error = "Failed to wait for 'Stakeholder Individual Known as' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_knownas(), testData.getData("Known as"))){
            error = "Failed to click 'Stakeholder Individual Known as' input field.";
            return false;
        } 
        //Stakeholder Individual title dropdown
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to wait for 'Stakeholder title' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title_dropdown())){
            error = "Failed to click 'Stakeholder title' dropdown.";
            return false;
        }
        //Stakeholder Individual title select
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to wait for 'Stakeholder title' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_title(testData.getData("Title")))){
            error = "Failed to select '"+testData.getData("Title")+"' from 'Title' options.";
            return false;
        }
        
        //Relationship owner
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to wait for 'Relationship owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdown())){
            error = "Failed to click 'Relationship owner' dropdown.";
            return false;
        }
        //Relationship owner select
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdownValue(testData.getData("Relationship owner")))){
            error = "Failed to wait for 'Relationship owner' options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_relationshipOwner_dropdownValue(testData.getData("Relationship owner")))){
            error = "Failed to select '"+testData.getData("Relationship owner")+"' from 'Relationship owner' options.";
            return false;
        }
        


        //Profile tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.profile_tab())){
//            error = "Failed to wait for Profile tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.profile_tab())){
//            error = "Failed to click Profile tab.";
//            return false;
//        }

        //Stakeholder Individual categorie 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 1")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        //Stakeholder Individual categorie 2
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 2")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 2")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
        //Stakeholder Individual categorie 3
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 3")))){
            error = "Failed to wait for Stakeholder categorie.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_stakeholdercat(testData.getData("Categorie 3")))){
            error = "Failed to click Stakeholder categorie.";
            return false;
        }
        
        //Stakeholder Individual Business Unit
        if(!testData.getData("Business Unit").equals("Global Company")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("Global Company"))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            if(!testData.getData("Business Unit").equals("South Africa")){
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to wait to expand 'South Africa'.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu_expand("South Africa"))){
                    error = "Failed to expand 'South Africa'.";
                    return false;
                }
                if(!testData.getData("Business Unit").equals("Victory Site")){
                    error = "Failed to find Business Unit";
                    return false;
                }else
                {
                    if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                        error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                }
            }else
            {
                if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                    error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
            }
        }else
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_appbu(testData.getData("Business Unit")))){
                error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
        }
        //Stakeholder Individual Impact Types
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_impacttypes_selectall())){
            error = "Failed to wait for Stakeholder Individual Impact Types.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_impacttypes_selectall())){
            error = "Failed to click Stakeholder Individual Impact Types select all.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Profile details entered.");
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        
        pause(15000);
//        //Stakeholder Individual Locations tab
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.location_tab())){
//            error = "Failed to wait for Location tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.location_tab())){
//            error = "Failed to click Location tab.";
//            return false;
//        }
//        //Stakeholder Individual Location select
//        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_locationmarker())){
//            error = "Failed to wait for Location Marker.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_locationmarker())){
//            error = "Failed to click Location Marker.";
//            return false;
//        }
//        //Sikuli 
//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//        
//        if (!sikuliDriverUtility.MouseClickElement(Stakeholder_Individual_PageObjects.si_location())) {
//            error = "Failed to click location on map.";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Location Selected.");
        //Navigate to Supporting Documents
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 6; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
        //Supporting Documents tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.supporting_tab())){
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.supporting_tab())){
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        //Add support document - Hyperlink
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.linkbox())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.linkbox())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.urlTitle(), getData("URL Title"))){
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
        
        //switch to the iframe
         if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            
        }
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Stakeholder_Individual_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
         //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        
        //Stakeholder details tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_detailsTab())){
            error = "Failed to wait for 'Stakeholder details tab' .";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_detailsTab())){
            error = "Failed to click 'Stakeholder details tab' .";
            return false;
        }
                //Stakeholder Individual designation
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_designation())){
            error = "Failed to wait for 'Stakeholder Individual Designation' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_designation(), testData.getData("Designation"))){
            error = "Failed to click 'Stakeholder Individual Designation' input field.";
            return false;
        } 
        //Stakeholder Individual DoB
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_dob())){
            error = "Failed to wait for 'Stakeholder Individual DoB' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_dob(), testData.getData("DoB"))){
            error = "Failed to click 'Stakeholder Individual DoB' input field.";
            return false;
        } 
        //Stakeholder Individual ID num
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_idnum())){
            error = "Failed to wait for 'Stakeholder Individual ID num' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_idnum(), testData.getData("ID num"))){
            error = "Failed to click 'Stakeholder Individual ID num' input field.";
            return false;
        } 
        //Stakeholder Individual gender dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_gender_dropdown())){
            error = "Failed to wait for gender dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_gender_dropdown())){
            error = "Failed to click gender dropdown.";
            return false;
        }
        //Stakeholder Individual gender select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_gender_select(testData.getData("Gender")))){
            error = "Failed to wait for gender dropdown option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_gender_select(testData.getData("Gender")))){
            error = "Failed to click gender dropdown option.";
            return false;
        }
        //Stakeholder Individual nationality dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_nationality_dropdown())){
            error = "Failed to wait for gender dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_nationality_dropdown())){
            error = "Failed to click gender dropdown.";
            return false;
        }
        //Stakeholder Individual nationality select
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_nationality_select(testData.getData("Nationality")))){
            error = "Failed to wait for Nationality dropdown option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_nationality_select(testData.getData("Nationality")))){
            error = "Failed to click Nationality dropdown option.";
            return false;
        }
 
        //Primary Contact number
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_primaryContactNum())){
            error = "Failed to wait for 'Stakeholder Individual Primary Contact number' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_primaryContactNum(), testData.getData("Primary Contact number"))){
            error = "Failed to click 'Stakeholder Individual Primary Contact number' input field.";
            return false;
        } 
        
        //Street Address
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_StreetAddress())){
            error = "Failed to wait for 'Stakeholder Individual Street Address' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_StreetAddress(), testData.getData("Street Address"))){
            error = "Failed to click 'Stakeholder Individual Street Address' input field.";
            return false;
        } 
        
        //Zip /Postal code
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_PostalCode())){
            error = "Failed to wait for 'Stakeholder Individual Postal code' input field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Stakeholder_Individual_PageObjects.si_PostalCode(), testData.getData("Postal code"))){
            error = "Failed to click 'Stakeholder Individual Postal code' input field.";
            return false;
        } 
        
        //Location
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_Location())){
            error = "Failed to wait for 'Stakeholder Individual Location' input field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_Location())){
            error = "Failed to click  'Stakeholder Individual Location' input field.";
            return false;
        }
        
         pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_Location_select(testData.getData("Location")))){
            error = "Failed to wait for Location dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_Location_select(testData.getData("Location")))){
            error = "Failed to click Location dropdown option.";
            return false;
        }
         
        //Training Management Tab
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 4; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_rightarrow())){
                error = "Failed to click right arrow.";
                return false;
            }
        }
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_TrainingManagementTab())){
            error = "Failed to wait for 'Stakeholder Individual Training Management Tab'.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_TrainingManagementTab())){
            error = "Failed to click  'Stakeholder Individual Training Management Tab'.";
            return false;
        }
         
       
         //Highest level of education
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_HighestLevelOfEducation())){
            error = "Failed to wait for 'Stakeholder Individual Highest level of education'.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_HighestLevelOfEducation())){
            error = "Failed to click  'Stakeholder Individual Highest level of education'.";
            return false;
        }
        
         pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_HighestLevelOfEducation_select(testData.getData("Highest level of education")))){
            error = "Failed to wait for Highest level of education dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_HighestLevelOfEducation_select(testData.getData("Highest level of education")))){
            error = "Failed to click Highest level of education dropdown option.";
            return false;
        }
         
         //Current job profile
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_CurrentJobProfile())){
            error = "Failed to wait for 'Stakeholder Individual Current job profile'.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_CurrentJobProfile())){
            error = "Failed to click  'Stakeholder Individual Current job profile'.";
            return false;
        }
        
         pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_CurrentJobProfile_select(testData.getData("Current job profile")))){
            error = "Failed to wait for Current job profile dropdown option.";
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_CurrentJobProfile_select(testData.getData("Current job profile")))){
            error = "Failed to click Current job profile dropdown option.";
            return false;
        }
         
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Stakeholder_Individual_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        pause(20000);
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.recordSaved_popup1());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        String[] stakeholderIndividualRecord = SeleniumDriverInstance.retrieveTextByXpath(Stakeholder_Individual_PageObjects.getStakeholderIndividualRecord()).split("#");
        setStakeholderRecordId(stakeholderIndividualRecord[1]);
        
        //Left chevron
         if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.sik_leftarrow())){
            error = "Failed to wait for left arrow.";
            return false;
        }
        for (int i = 0; i < 4; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.sik_leftarrow())){
                error = "Failed to click left arrow.";
                return false;
            }
        }
        
        pause(5000);
        //Stakeholder details tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Stakeholder_Individual_PageObjects.si_detailsTab())){
            error = "Failed to wait for 'Stakeholder details tab' .";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Stakeholder_Individual_PageObjects.si_detailsTab())){
            error = "Failed to click 'Stakeholder details tab' .";
            return false;
        } 
        
        //Scroll down
         JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
         js.executeScript("window.scrollBy(0,1000)");
         
          pause(2000);
          narrator.stepPassedWithScreenShot("Successfully updated on Map");
         
          pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
         //switch to the iframe
          if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame "; 
        }
          
        return true;
    }

}
